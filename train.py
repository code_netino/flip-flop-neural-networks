from torchvision import transforms
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.nn.functional as F
#import dataset_processing
import torch.optim as optim
from torch.autograd import Variable
import torch
import numpy as np
import os
import timeit
from tqdm import tqdm
from statistics import mean
import csv
from SR_remodel import SR
import glob
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from dataloader import Days
import pandas as pd
from torch.utils.model_zoo import load_url as load_state_dict_from_url
from sklearn.model_selection import train_test_split

"""
Will be Updating the code with LR Scheduler for decay the LR

"""


def build_timeseries(mat, y_col_index):
    TIME_STEPS=60
    dim_0 = mat.shape[0] - TIME_STEPS
    dim_1 = mat.shape[1]
    x = np.zeros((dim_0, TIME_STEPS, dim_1))
    y = np.zeros((dim_0,))
    
    for i in tqdm(range(dim_0)):
        x[i] = mat[i:TIME_STEPS+i]
        y[i] = mat[TIME_STEPS+i, y_col_index]
    print("length of time-series i/o",x.shape,y.shape)
    return x, y

def trim_dataset(mat, batch_size):
    """
    trims dataset to a size that's divisible by BATCH_SIZE
    """
    no_of_rows_drop = mat.shape[0]%batch_size
    if(no_of_rows_drop > 0):
        return mat[:-no_of_rows_drop]
    else:
        return mat

def train_model(train_dataloader,val_dataloader,num_epochs=20,bs=16):    

    train_loader=DataLoader(dataset=train_dataloader,batch_size=bs,shuffle=False,num_workers=4)
    test_loader=DataLoader(dataset=val_dataloader,batch_size=bs,shuffle=False,num_workers=4)
    seq_model=SR(1,40,40,20,10,1)
    seq_model.cuda()
    loss_function=nn.MSELoss()
    optimizer= torch.optim.Adagrad(seq_model.parameters(), lr=0.001)
    trainvaltest_loaders = {'train': train_loader, 'val': test_loader}
    loss_list=[]
    val_loss=10000
    for epoch in range(0, num_epochs):
        running_loss=0.0
        for phase in ['train','val']:
            start_time = timeit.default_timer()

            if phase == 'train':
                seq_model.train()
                optimizer.zero_grad()
            else:
                seq_model.eval()

            for i,data in enumerate(tqdm(trainvaltest_loaders[phase])):
                inputs,preds=data
                inputs,preds=inputs.float().cuda(),preds.float().cuda()
                preds=preds.reshape([preds.shape[0],1])
                inputs = Variable(inputs, requires_grad=True)
                preds= Variable(preds,requires_grad=False)
                if(phase == 'train'):  
                    output,memory=seq_model(inputs)
                    single_loss=loss_function(output,preds)
                else:
                    with torch.no_grad():
                        output,val_memory=seq_model(inputs)
                        single_loss=loss_function(output,preds)
                if phase == 'train':
                    seq_model.backward(single_loss,output,preds,inputs)
                    optimizer.step()
                else:
                    running_loss += single_loss.item()
        if(((running_loss)/len(trainvaltest_loaders['val']))<val_loss):
            val_loss=(running_loss/len(trainvaltest_loaders['val']))
            torch.save(seq_model.state_dict(),r'C:\Users\Sujith\Stock_Prediction\sr_ff.pth')
            print('Saving Model at Epoch: ',epoch)
        else:
            pass
        print('Epoch :',epoch,' Loss : ',(running_loss/len(trainvaltest_loaders['val'])))
        loss_list.append(single_loss.item())
        plt.title('Model Val loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.plot(list(range(1,201)),loss_list)
    plt.legend(['Val'], loc='upper right')
    plt.show()
    plt.imshow()

def main():
    df_ge = pd.read_csv('ge.txt.txt')
    train_cols = ["Open","High","Low","Close","Volume"]
    df_train, df_test = train_test_split(df_ge, train_size=0.8, test_size=0.2, shuffle=False)
    x = df_train.loc[:,train_cols].values
    min_max_scaler = MinMaxScaler()
    x_train = min_max_scaler.fit_transform(x)
    x_test = min_max_scaler.transform(df_test.loc[:,train_cols])
    x_t, y_t = build_timeseries(x_train, 3)
    x_t = trim_dataset(x_t, 16)
    y_t = trim_dataset(y_t, 16)
    x_temp, y_temp = build_timeseries(x_test, 3)
    x_val, x_test_t = np.split(trim_dataset(x_temp, 16),2)
    y_val, y_test_t = np.split(trim_dataset(y_temp, 16),2)
    train_dloader=Days(x_t,y_t)
    val_dloader=Days(x_val,y_val)
    train_model(train_dloader,val_dloader)
if __name__ == "__main__":
    main()
