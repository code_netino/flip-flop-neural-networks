# Flip Flop Neural Networks

This is the Implementation of Flip Flop Neural Netwokrs, a new breed of memory based Networks which can be used for Sequential Problems. The Flip Flop Neural Networks are dependent on the functioning of fundamental flip flops namely the SR and JK flip flops. Flip Flops are basicially electrical latch components which have the abilty to hold the information from the previous time steps as a memory component and use it for the prediction O/P in the future time steps. The SR Flip Flop Model was applied to wide range of sequence prediction tasks such as Stock price prediction,Power Consumption Prediction etc. and the results were extracted. Moreover, the performance of the Flip Flop Neural Networks have found to be superior in terms of prediction results and Evaluation metrics in comparision to that of the standard Long Short Term Memory(LSTM) modules.

- SR_remodel.py - Implementation of the SR flip flop memory model  
- jk_new.py - Implementation of the JK flip flop memory model
- train.py - Implementation of Stock price prediction using the SR Flip Flop memory model

Some of the libraries used for this Implementation:-

- Pytorch  
- Numpy
- Seaborn
- Matplotlib
- Pandas

Flip Flop Network architecture is given below:-

![Flip flop Networks Architecture](ffnn.PNG)


### Publication

The derivation of the dynamic memory equation, the architecture details and the obtained results have been documented as a research paper and has been **Presented** at the Conference **Machine learning, Deep learning and Computational Intelligence for wireless communication(MDCWC), October,2020**. **Springer - https://doi.org/10.1007/978-981-16-0289-4_13**.








